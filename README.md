## querySelector & querySelectorAll
documentation : https://developer.mozilla.org/fr/docs/Web/API/Document/querySelector

Dans cet excercice nous utilisons **querySelector** pour selectionner notre formulaire (élément **form**) que nous gardons dans la variable **myForm** :

````
var myForm = document.querySelector('form');
````

Pour récuperer la liste des champs input nous utilisons la méthode  **querySelectorAll**
doc : https://developer.mozilla.org/fr/docs/Web/API/Document/querySelectorAll

````
var inputs = document.querySelectorAll('form input:not([type="submit"])');
````

**querySelectorAll** nous retourne un tableau d'élément alors que **querySelector** nous retourne l'élément directement.