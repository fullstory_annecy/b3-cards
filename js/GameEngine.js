class GameEngine {
    // Player player1
    // Player player2
    constructor(player1, player2, cardTypes) {
        this.currentRound = -1;
        this.players = [];
        this.players.push(player1);
        this.players.push(player2);

        this.roundCards = [];

        // events
        this.onCardSelectedBinded = this.onCardSelected.bind(this);

        // ID of defense timer
        this.idTimeoutDefense = -1;

        this.cardsTypes = cardTypes;
        this.cards = [];
        this.generateGame();
       //

    }

    giveCards() {
        const MaxCard = 3;
        for (var i = 0; i < this.players.length; i++) {

            while (this.cards.length && this.players[i].cards.length < MaxCard) {
                this.players[i].addCard(this.cards.pop());
            }

            this.players[i].showCards();
            console.log('GameEngin : giveCards status amount of card left',this.players[i].cards)
        }

    }

    generateGame() {

        for (var i = 0; i < 20; i++) {
            var randomType = this.cardsTypes[Math.floor(Math.random() * this.cardsTypes.length)];

            switch (randomType) {
                case 'AttackCard' :
                    this.cards.push(new AttackCard(i));
                    break;
                case 'DefenseCard' :
                    this.cards.push(new DefenseCard(i));
                    break;
            }
        }

        this.displayCards();
    }

    nextRound() {

        for (let i = 0; i < this.players.length; i++) {
            console.log('status score : ',this.players[i].selector,this.players[i].getPoints());
            console.log('////')
            if (this.players[i].getPoints() <= 0) {
                console.log('status defeat PLAYER ',this.players[i].selector , ' LOOSE');
                return;
            }
        }

        // we go to next round
        this.currentRound++;


        // we clean the board

        for (let i = 0 ; i < this.roundCards.length ; i++) {
            if(this.roundCards[i]) {
                this.roundCards[i].hide();
            }
        }

        this.roundCards = [];

        console.log('GameEngine : nextRound', this.currentRound);

        this.giveCards();

        for (let i = 0; i < this.players.length; i++) {
            console.log('status card count : ',this.players[i].selector,this.players[i].cards.length);

        }

        this.givePlayerHand(this.players[this.currentRound % this.players.length], "all");


    }

    givePlayerHand(player, type) {
        console.log('GameEngine : givePlayerHand', player.selector, type, player.cards.length);
            player.ui.addEventListener('card', this.onCardSelectedBinded);
            // ask the player to select a card
            player.chooseCard(type);
    }

    onCardSelected(e) {
        console.log("GameEngine : onCardSelected", e.detail.card);
        clearTimeout(this.idTimeoutDefense);

        e.currentTarget.removeEventListener('card', this.onCardSelectedBinded);
        this.roundCards.push(e.detail.card);
        this.checkCards();
    }

    checkCards() {


        setTimeout(() => {



        var currentPlayer = this.players[(this.currentRound) % this.players.length];
        var nextPlayer = this.players[(this.currentRound + 1) % this.players.length];

        console.log('GameEngine : checkCards', this.roundCards.length);

        switch (this.roundCards.length) {
            case 1 :

                if (this.roundCards[0] && this.roundCards[0].getType() === "attack") {
                    console.log('GameEngine : checkCards ATTACK', this.roundCards[0]);
                    // next player will have 5000ms to defend himself
                    this.idTimeoutDefense = setTimeout(() => {
                        console.log('GameEngine : checkCards no defense after 5s...');
                        this.roundCards.push(null);
                        this.checkCards();

                    }, 3000);
                    console.log('GameEngine : checkCards defense Timer Launch');

                    // we give the next player the opportunity to defend himself
                    var r = Math.random() - Math.random();
                    if (r > 0) {
                        console.log('GameEngine : checkCards yes fight back!!!!');
                        this.givePlayerHand(nextPlayer, "defense");
                    }

                } else if(this.roundCards[0] && this.roundCards[0].getType() === "defense") {
                    console.log('GameEngine : checkCards currentPlayer defend himself, next round');
                    currentPlayer.addPoints(this.roundCards[0].getPoints());
                    this.nextRound();

                }else {
                   // this.nextRound();
                    var emptyCount = 0;
                    for (var i = 0; i < this.players.length; i++) {
                        console.log('status card count else : ',this.players[i].selector,this.players[i].getPoints());
                        emptyCount++;
                    }
                    if(emptyCount === 2) {
                        console.log('GAME END NO MORE CARDS');
                    }
                }

                break;
            case 2 :
                // Check Point

                if (this.roundCards[1]) {
                    var attackPoint = this.roundCards[0].getPoints() - this.roundCards[1].getPoints();
                    console.log("GameEngine checkCards CheckPoints DEFENSE", Math.max(0, attackPoint))
                    nextPlayer.addPoints(-Math.max(0, attackPoint));
                    console.log('status battle')
                } else {

                    if (this.roundCards[0].getType() === "attack") {
                        nextPlayer.addPoints(-this.roundCards[0].getPoints());
                        console.log('status no battle attack, we remove points to next player')
                    } else {
                        currentPlayer.addPoints(this.roundCards[0].getPoints());
                        console.log('status no battle defense, we add points to current player')
                    }

                }

                this.nextRound();

                break;
        }
        },1000);
    }

    // UI
    displayCards() {
        var tl = new TimelineMax({delay : 1});
        for (var i = 0 ; i < this.cards.length ; i++) {
            this.cards[i].render();
            document.body.append(this.cards[i].ui);
            tl.fromTo(this.cards[i].ui, .1, {left : '50%', opacity : 0, zIndex : i,x : '-20%', y : i/10,scale : 1.1},{scale : 1,y : 0,x : -50+ i/2 +'%',startAt : {opacity : 1},opacity : 1});
        }
        tl.addCallback(() => {
            this.nextRound();
        })
    }

}