let player1 = new Player('#player1');
let player2 = new Player('#player2');

let cardTypes = [
    'AttackCard',
    'DefenseCard'
];

let gameEngine = new GameEngine(player1,player2,cardTypes);
