class Player {
    constructor(selector) {
        this.selector = selector;
        this.ui = document.querySelector(this.selector);
        this.scoreUI = this.ui.querySelector('span');
        // .score
        // .cardHolder
        // .changeCards
        this.points = 20;
        this.scoreUI.textContent = this.points;
        this.cards = [];
    }

    reset() {

    }

    addCard(card) {
        console.log('root add card');
        this.cards.push(card);
    }
    getPoints() {
        return this.points;
    }

    addPoints(points) {
        this.points += points;
        this.points = Math.min(this.points,30);

        this.scoreUI.textContent = this.points;

        let anim = points > 0 ?  this.ui.querySelector('.positive') : this.ui.querySelector('.negative');
        anim.textContent = points;
        let tl = new TimelineMax();
        tl.to(anim,.5,{opacity : 1, y : '-=40'});
        tl.to(anim,.5,{opacity : 1});
        tl.set(anim,{opacity : 0, y : 0});

        console.log('Player : addPoints ',this.selector,points, 'totalPoints:',this.points);
    }

    chooseCard(type) {
        setTimeout(() => {
        var tempCards = this.cards.filter(card => card.getType() === type || type === "all");
        console.log('Player : chooseCard',type,tempCards,this.cards.length );

        let selectedCard;

        if(tempCards.length) {

            let randomCard = tempCards[Math.floor(Math.random() * tempCards.length)];

            for (var i = 0 ; i < this.cards.length ; i++) {
                if(this.cards[i].id === randomCard.id) {
                    selectedCard = this.cards.splice(i,1)[0];
                }
            }

            console.log('check selectedCard',selectedCard,this.cards);

        }else {
            selectedCard = null;
        }


            this.ui.dispatchEvent(new CustomEvent('card', {
                detail : {
                    card :selectedCard
                }
            }));

        if(selectedCard) {
            selectedCard.moveTo(window.innerWidth/2,window.innerHeight/2,0);
        }

        },1000);



    }

    // UI

    showCards() {
        let position = this.ui.getBoundingClientRect();
        this.cards.forEach((card,index) => {
            card.moveTo(position.left + position.width/3, position.top + position.height/2,index);
        })
    }
}