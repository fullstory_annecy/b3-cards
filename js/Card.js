class Card {

    constructor(id) {
        this.id = id;
        this.ui = document.createElement('div');
        this.ui.setAttribute('class','card');

        var span = document.createElement('span');
        this.points = 1 + Math.round(Math.random() * 10);
        this.type = '';
        span.textContent = this.points;
        this.ui.appendChild(span);
        this.isRevealed = false;
    }

    render() {
        var h2 = document.createElement('h2');
        h2.textContent = this.type;
        this.ui.querySelector('span').append(h2);
    }

    action() {

    }

    getType() {
        return this.type;
    }

    getPoints() {
        return this.points;
    }

    destroy() {
        // remove card from table
    }

    // UI
    reveal() {
        if(! this.isRevealed) {
            this.isRevealed = true;
            let tl = new TimelineMax({delay : .2});
            tl.to(this.ui, .3, {rotationY : 90, ease : Power3.easeIn});
            tl.set(this.ui.querySelector('span'), {opacity : 1});
            tl.to(this.ui, .3, {rotationY : 180,ease : Power3.easeOut});
        }
    }

    hide() {
        TweenMax.to(this.ui, .3, {opacity : 0, y : '+=20', ease : Power3.easeIn});
    }

    moveTo(x,y,index) {
        var tl = new TimelineMax();
        tl.to(this.ui,.3, {delay : index/5,left : (x + index * 80), top : y });
        tl.addCallback(() => {
            this.reveal();
        })
    }

}
